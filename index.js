const getCurrentDateString = () => {
  const currentDate = new Date();
  return currentDate.toDateString() + " " + currentDate.toLocaleTimeString();
};
const getTimeString = (date) => {
  let hours = date.getHours();
  let minutes = date.getMinutes();
  let ampm = hours >= 12 ? "PM" : "AM";
  hours = hours % 12;
  hours = hours ? hours : 12;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  let strTime = hours + ":" + minutes + " " + ampm;
  return strTime;
};
const calc = (minute) => {
  if (minute > 0 && minute <= 15) return 500;
  else if (minute > 15 && minute <= 30) return 1000;
  else if (minute > 30 && minute <= 60) return 1500;
  else if (minute > 60) return 1500 + calc(minute - 60);
  else return 0;
};
const calculate = (start, stop) => {
  const totalTime = stop - start;
  const totalMinute = Math.floor(totalTime / 60000);
  const totalRiel = calc(totalMinute);

  return {
    totalMinute,
    totalRiel,
  };
};
let startTime;
let stopTime;
const start = document.getElementById("start");
const pause = document.getElementById("stop");
const clear = document.getElementById("clear");
const startAt = document.getElementById("start-at");
const stopAt = document.getElementById("stop-at");
const minute = document.getElementById("minute");
const riel = document.getElementById("riel");

const liveTime = setInterval(() => {
  document.getElementById("date-time").innerHTML = getCurrentDateString();
}, 1000);

start.onclick = () => {
  startTime = new Date();
  startAt.innerHTML = getTimeString(startTime);
  start.classList.remove("show");
  pause.classList.add("show");
};
pause.onclick = () => {
  stopTime = new Date();
  stopAt.innerHTML = getTimeString(stopTime);
  const { totalMinute, totalRiel } = calculate(startTime, stopTime);
  minute.innerHTML = totalMinute;
  riel.innerHTML = totalRiel;
  pause.classList.remove("show");
  clear.classList.add("show");
};
clear.onclick = () => {
  clear.classList.remove("show");
  start.classList.add("show");
  startAt.innerHTML = "0:00";
  stopAt.innerHTML = "0:00";
  minute.innerHTML = "0";
  riel.innerHTML = "0";
  startTime = null;
  stopTime = null;
};
